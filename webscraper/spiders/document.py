# -*- coding: utf-8 -*-
import os
import scrapy
from urlparse import urlparse


class DocumentSpider(scrapy.Spider):
    name = "document"
    allowed_domains = ["sites.google.com"]
    start_urls = (
        'https://sites.google.com/site/wikitraumaca/',
    )


    def parse(self, response):
        for url in response.css('ul.sites-header-nav-container-boxes li a::attr("href")'):
            yield scrapy.Request(response.urljoin(url.extract()), self.dig_pages)


    def dig_pages(self, response):
        if response.css('div.sites-subpages').extract_first() is None:
            # Then there is no sublinks so we need to store that page for
            # further processing
            filename = urlparse(response.url).path + '.html'
            if not os.path.exists(os.path.dirname('pages'+filename)):
                os.makedirs(os.path.dirname('pages'+filename))
            with open('pages'+filename, 'wb') as f:
                f.write(response.body)
        else:
            for url in response.css('div.sites-subpages span a::attr("href")'):
                yield scrapy.Request(response.urljoin(url.extract()), self.dig_pages)
